<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
   pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<% String username=(String) session.getAttribute("userName");
session = request.getSession();
if(username!=null){
	response.sendRedirect("userpage.jsp");
}
%>	
   <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Login</title>
</head>
<center>
<body>
   <h3>Please enter your UserName number and password </h3>
      <s:form action="login">
          <s:textfield name="userName" label="Enter User Name" />
          <s:password name="password" label="Enter Password" />
          <s:submit label="index" value="Login"/>
      </s:form>
</body>
</center>
</html>