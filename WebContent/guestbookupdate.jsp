<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd"> 
<html>
<head>
<title>Guestbook Save</title>
</head>
<center>
<body>
<%
String username=(String) session.getAttribute("userName");
session = request.getSession();
if (username==null){
	response.sendRedirect("error.jsp");
  }else{%>
   <h1>Welcome<%= session.getAttribute("userName")%></h1>
<h1>Guestbook Save</h1>
 <s:form action="ModifyGuestbook">
	 <table border="1px" cellpadding="8px">
		<s:hidden name="id"/>
		<s:hidden name="username"/>
	 	<tr>
			<s:textfield name="username" label="Username" disabled="true"/>
		</tr>
		<tr>
			<s:textfield name="title" label="Guest Title" />
		</tr>
		<tr>
			<s:textfield name="message" label="Guest Message" />	
		</tr>
		<tr>
		    <s:submit label="sub" value="submit"/>
		</tr>
	 </table>
 </s:form>
 <br>

</body>
<%}%>
</center>
</html>