<%@ page language="java" contentType="text/html; charset=BIG5"
    pageEncoding="BIG5"%>
<!DOCTYPE html>
<% 
String username=(String) session.getAttribute("userName");
session = request.getSession();
response.setHeader("Refresh", "3; url=index.jsp");
%>
<html>
<head>
<meta charset="BIG5">
<title>Error</title>
</head>
<body>
	<%if(username==null){ %>
		<p>Your session expires</p>
		<p>After three seconds back Login</p>
	<%} else {%>
		<p>Oops!Your account number or password is incorrect</p>
		<p>After three seconds back Login</p>
	<%}%>
</body>
</html>