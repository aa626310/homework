<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="s" uri="/struts-tags" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd"> 
<html>
<head>
<title>Guestbook Title</title>
</head>
<center>
<body>
<%
String username=(String) session.getAttribute("userName");
session = request.getSession();
if (username==null){
	response.sendRedirect("error.jsp");
  }else{%>
   <h1>Welcome <%= session.getAttribute("userName")%></h1>
<h1>Guestbook</h1>
<h2><a href="userpage.jsp">cheek me</a></h2>
 <s:form action="AddGuestbook">
	 <table border="1px" cellpadding="8px">
	 	<s:hidden name="username"/>
	 	<tr>
			<s:textfield name="username" label="Username" disabled="true" />
		</tr>
		<tr>
			<s:textfield name="title" label="Guest Title" value="" />
		</tr>
		<tr>
			<s:textfield name="message" label="Guest Message" value=""/>	
		</tr>
		<tr>
			<s:submit label="sub" value="submit"/>
		</tr>
	 </table>
 </s:form>
 <br>
	 <table border="1px" cellpadding="8px">
		<tr>
			<th>ID</th>
			<th>Username</th>
			<th>Title</th>
			<th>Message</th>
			<th>Time</th>
			<th>Delete</th>
			<th>Modify</th>
		</tr>
		<s:iterator value="guestbookList" status="userStatus">
		
			<tr>
				<td><s:property value="id" /></td>
				<td><s:property value="username" /></td>
				<td><s:property value="title" /></td>
				<td><s:property value="message" /></td>
				<td><s:date name="time" format="dd/MM/yyyy" /></td>
				<td>
					<s:if test="username==#session.userName">
						<a href="<s:url action="DeleteGuestbook?id=%{id}&username=%{username}"/>">Delete</a>
					</s:if>	
				</td>
				<td>
					<s:if test="username==#session.userName">
						<a href="<s:url action="TempGuestbook?id=%{id}&username=%{username}&title=%{title}&message=%{message}"/>">Modify</a>
					</s:if>	
				</td>
			</tr>
		</s:iterator>
	</table>
</body>
<%}%>
</center>
</html>