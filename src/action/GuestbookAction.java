package action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import model.Guestbook;

@SuppressWarnings({ "rawtypes" })
public class GuestbookAction extends ActionSupport implements ModelDriven{

	private static final long serialVersionUID = 1L;
	Guestbook guestbook = new Guestbook();
	
	List<Guestbook> guestbookList = new ArrayList<Guestbook>();

	public String execute() throws Exception {
		return SUCCESS;
	}
	
	public Object getModel() {
		return guestbook;
	}
	
	public List<Guestbook> getGuestbookList() {
		return guestbookList;
	}
	
	public void setGuestbookList(List<Guestbook> guestbookList) {
		this.guestbookList = guestbookList;
	}
	
//save Guestbook
@SuppressWarnings("unchecked")
public String AddGuestbook() throws Exception{
	
	Configuration configuration= new Configuration();
    Configuration configure=configuration.configure("hibernate.cfg.xml");
    SessionFactory sessionFactory = configure.buildSessionFactory();
    
	Session session = sessionFactory.openSession();

	//save it
	guestbook.setTime(new Date());
 
	session.beginTransaction();
	session.save(guestbook);
	session.getTransaction().commit();
 
	//reload the Guestbook list
	guestbookList = null;
	
	guestbookList = session.createQuery("from Guestbook").list();
	
	return SUCCESS;

}

//Delete Guestbook
@SuppressWarnings({ "unchecked"})
public String DeleteGuestbook() throws Exception{
	
	Configuration configuration= new Configuration();
    Configuration configure=configuration.configure("hibernate.cfg.xml");
    SessionFactory sessionFactory = configure.buildSessionFactory();
    
	Session session = sessionFactory.openSession();

	//Delete it
	guestbook.setId(guestbook.getId());
	
	session.beginTransaction();
	session.delete(guestbook);
	session.getTransaction().commit();
 
	//reload the Guestbook list
	guestbookList = null;
	guestbookList = session.createQuery("from Guestbook").list();
	
	return SUCCESS;

}

//Delete Guestbook
@SuppressWarnings({ "unchecked"})
public String ModifyGuestbook() throws Exception{
	
	Configuration configuration= new Configuration();
	Configuration configure=configuration.configure("hibernate.cfg.xml");
	SessionFactory sessionFactory = configure.buildSessionFactory();
  
	Session session = sessionFactory.openSession();

	//modify it
	guestbook.setId(guestbook.getId());
	guestbook.setTime(new Date());
	
	session.beginTransaction();
	session.update(guestbook);
	session.getTransaction().commit();

	//reload the Guestbook list
	guestbookList = null;
	guestbookList = session.createQuery("from Guestbook").list();
	
	return SUCCESS;

}

//list all Guestbooks
@SuppressWarnings("unchecked")
public String listGuestbook() throws Exception{
	
	//get hibernate session from the servlet context
	Configuration configuration= new Configuration();
    Configuration configure=configuration.configure("hibernate.cfg.xml");
    SessionFactory sessionFactory = configure.buildSessionFactory();
	Session session = sessionFactory.openSession();

	guestbookList = session.createQuery("from Guestbook").list();
	
	return SUCCESS;

}

public String TempGuestbook(){
	guestbook.setId(guestbook.getId());
	guestbook.setUsername(guestbook.getUsername());
	return SUCCESS;
}
}
