package action;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.components.Password;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;

import java.util.List;
import java.util.Map;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import model.User;

@SuppressWarnings("unused")
public class LoginAction extends ActionSupport implements SessionAware {
    
	private static final long serialVersionUID = 1L;
	private Map<String, Object> sessionMap;
    private String userName;
    private String password;
    
    
    @Override
    public void setSession(Map<String, Object> sessionMap) {
        this.sessionMap = sessionMap;
    }
     
    public void setUserName(String userName) {
        this.userName = userName;
    }
     
    public void setPassword(String password) {
        this.password = password;
    }
    
    @SuppressWarnings("deprecation")
	public String login() {
        String loggedUserName = null;
        
        // check if the userName is already stored in the session
        if (sessionMap.containsKey("userName")) {
            return SUCCESS;
        }
         
        // if no userName stored in the session,
        // check the entered userName and password
        Configuration configuration= new Configuration();
        Configuration configure=configuration.configure("hibernate.cfg.xml");
        SessionFactory sessionFactory = configure.buildSessionFactory();
        Session session =  sessionFactory.openSession();
        User user = null;
        try {
        	user = (User) session.createCriteria(User.class).add(Restrictions.eq("account", userName)).list().get(0);
        }catch(Exception e) {
        	;
        }
        
        if (user != null && password != null) {
             
            // add userName to the session
        	ActionContext context = ActionContext.getContext();
        	sessionMap = context.getSession();
            sessionMap.put("userName",userName);
             
            return SUCCESS; // return welcome page
        }
         
        // in other cases, return login page
        return ERROR;
    }
     
    public String logout() {
        // remove userName from the session
        if (sessionMap.containsKey("userName")) {
            sessionMap.remove("userName");
        }
        return LOGIN;
    }
 
    
    
}

