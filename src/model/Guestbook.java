package model;

import java.util.Date;

public class Guestbook implements java.io.Serializable {
	private static final long serialVersionUID = 1L;
	public int id;
	public String username;
	public String title;
	public String message;
	public Date time; 
	
	public Guestbook() {   
    }
	
	public Guestbook(int id,String username, String title,String message, Date time) {   
		this.id = id;
		this.title = title;
        this.username = username;   
        this.message = message;   
        this.time = time;   
    }
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getTitle() {   
        return this.title;   
    }   
    public void setTitle(String title) {   
        this.title = title;   
    }
}
